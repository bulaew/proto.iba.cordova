It's really simple apps, but for many many users on logistics company

Simple mobile/tablet and in future dekstop app with very simple use case - 

1. Select Task (click on button for example), 

2. Open simple form, for first version one text box and store it as json 

3. add Photo, Video or Barcode (if you make photo, check if is barcode on photo), and store all locally on device
	
Cordova + Vue.js + PouchDB for apps and Node.JS + PostgreSQL (or REDIS for test) + Fabric

**Vue.js App:**

Android app supported this barcodes:
QR_CODE,DATA_MATRIX,UPC_E,UPC_A,EAN_8,EAN_13,CODE_128,CODE_39,CODE_93,CODABAR,ITF,RSS14,RSS_EXPANDED,PDF417,AZTEC
Not by default, but supported if you pass in the "formats" option:
PDF417
AZTEC

Cordova prepare projects

cd node-postgres-server

cordova create task-list-client iba.cz.by Task-List

cordova platform add android --save

Add cordova plugins by the commands:

+ 
cordova plugin add cordova-plugin-network-information

- 
cordova plugin add cordova-plugin-crosswalk-webview

+ 
cordova plugin add cordova-plugin-file

+
cordova plugin add cordova-plugin-camera

+ 
cordova plugin add cordova-plugin-media-capture

- 
cordova plugin remove cordova-plugin-media-capture-mp4video

+ 
cordova plugin add phonegap-plugin-barcodescanner

+ 
cordova plugin add cordova-plugin-whitelist

+
cordova plugin add cordova-fabric-plugin --variable FABRIC_API_KEY=3495775ebd2f81fc863261ff6937ea40a940c468 --variable FABRIC_API_SECRET=1ffe75f375d792d1ff269a29074fa2a1dfd04654afd9a9dc00d4caa477b8abeb

+
cordova plugin add phonegap-plugin-push --variable SENDER_ID="104270858389"

sudo apt-get install libgif-dev

fix vuejs-svg-icon in node_module

cordova run android



**Node.js App:**

postgresql tutorial: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-centos-7
string connection: postgres://test1:test1@127.0.0.1:5432/postgres

cd node-postgres-server

npm install

npm install supervisor@0.11.0 -g

node models/database.js

npm run start

Then navigate to http://localhost:3000/ in your browser. You should see the “Welcome to 
Express” text.
Test the endpoint(read) out in your browser at http://localhost:3000/api/v1/tasks