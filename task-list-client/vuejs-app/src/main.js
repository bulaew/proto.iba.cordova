import Vue from 'vue'
import App from './App'

import VueResource from 'vue-resource';
Vue.use(VueResource);

import PhotoView from './components/PhotoView';
Vue.component('PhotoView', PhotoView);

import VueCropper from 'vue-cropperjs';
Vue.component('VueCropper', VueCropper);

import 'mint-ui/lib/style.css';
import { InfiniteScroll } from 'mint-ui';
Vue.use(InfiniteScroll);
import { Cell } from 'mint-ui';
Vue.component(Cell.name, Cell);
import { Popup } from 'mint-ui';
Vue.component(Popup.name, Popup);
import { Spinner } from 'mint-ui';
Vue.component(Spinner.name, Spinner);
import { Button } from 'mint-ui';
Vue.component(Button.name, Button);
import { PaletteButton } from 'mint-ui';
Vue.component(PaletteButton.name, PaletteButton);
import { Field } from 'mint-ui';
Vue.component(Field.name, Field);
import { Header } from 'mint-ui';
Vue.component(Header.name, Header);
import { Actionsheet } from 'mint-ui';
Vue.component(Actionsheet.name, Actionsheet);
import { Checklist } from 'mint-ui';
Vue.component(Checklist.name, Checklist);

import Icon from 'vue-svg-icon/Icon';
Vue.component('icon', Icon);  
Icon.inject('photo'); 
Icon.inject('crop'); 
Icon.inject('video'); 
Icon.inject('barcode'); 
Icon.inject('done'); 
Icon.inject('delete');
Icon.inject('remove'); 
Icon.inject('add'); 
Icon.inject('sort'); 
Icon.inject('update'); 
Icon.inject('move'); 
Icon.inject('crop'); 
Icon.inject('close'); 
Icon.inject('rotate'); 

 

import {LocalDb} from './db/localDb';
import {RemoteDb} from './db/remoteDb';

let tasks = [];
let spinner = { visible: false }
let localDb = new LocalDb(tasks); 
let remoteDb = new RemoteDb(tasks, spinner);

new Vue({
	el: '#app',
	components: { App },
	template: '<App v-bind:tasks="tasks" v-bind:localDb="localDb" v-bind:remoteDb="remoteDb" v-bind:spinner="spinner"/>',
	created: function() {
        console.log("created #app");
		// localDb.read();
	},
	data: {
		tasks,
		localDb,
		remoteDb,
		spinner
	}
})

