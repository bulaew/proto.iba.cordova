import SHA256 from 'crypto-js/sha256';

export class RemoteDb {
	constructor(tasks, spinner) {
		this.tasks = tasks;
		this.spinner = spinner;
		this.auth = false;	
		this.token = {};
		this.name = '';
		this.admin = false;
		this.part = 0;
		this.size = 20;
	}

	isAuth() {
		if (!this.name) throw "error: no auth";
	}

	isOnLine() {
		if (!navigator.onLine) throw "error: no internet connection";
	}

	login(name, password) {
		return new Promise((resolve, reject) => {
			try {
				this.isOnLine();

				let xhttp = new XMLHttpRequest();
				password = '' + SHA256(password);

				xhttp.onreadystatechange = () => {
					if (xhttp.readyState === 4) {
						this.spinner.visible = false;
						
						if (xhttp.status == 401) {
							alert("Your login/password is incorrect",null,"Authentication Error","OK");
							return;
						}
						
						if (xhttp.status === 200) {
							let response = JSON.parse(xhttp.responseText);
							this.auth = true;
							this.name = response.name;
							this.admin = response.admin;
							this.token = response.token;							
							resolve();
						}
					};
				};
				xhttp.open("POST", "http://52.32.192.195:3003/userList/login");
				xhttp.setRequestHeader('Content-Type', 'application/json');
				xhttp.send(JSON.stringify({
					name: name,
					password: password
				}));
				this.spinner.visible = true;
			} catch (e) {
				alert(e);
			}
		})
	}

	logout() {
		this.auth =  false;						
		this.token = {};
		this.name = '';
		this.admin = false;	
	}

	create(index) {
		try {
			this.isAuth();
			this.isOnLine();

			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = () => {
				if (xhttp.readyState === 4 && xhttp.status === 200) {
					let task = JSON.parse(xhttp.responseText);
					this.tasks[index] = task;

					this.spinner.visible = false;
				};
				if (xhttp.readyState === 4 && xhttp.status !== 200) {
					this.spinner.visible = false;
					alert("Error. Status code: " + xhttp.status);
				}
			};
			xhttp.open("POST", "http://52.32.192.195:3003/taskList");
			xhttp.setRequestHeader('Content-Type', 'application/json');
			xhttp.setRequestHeader('token', this.token);
			xhttp.send(JSON.stringify(this.tasks[index]));
			this.spinner.visible = true;
		} catch(e) {
			alert(e);
		}
	}

	read() {
		try {
			this.isAuth();
			this.isOnLine();
		
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = () => {
				if (xhttp.readyState === 4 && xhttp.status === 200) {
					let gotTasks = JSON.parse(xhttp.responseText);

					this.tasks.splice(0, this.tasks.length);
					gotTasks.map((gotTask) => {
						this.tasks.push(gotTask);
					});
					this.spinner.visible = false;
				};
				if (xhttp.readyState === 4 && xhttp.status !== 200) {
					this.spinner.visible = false;
					alert("Error. Status code: " + xhttp.status);
				}
			};
			xhttp.open("GET", "http://52.32.192.195:3003/taskList");
			xhttp.setRequestHeader('token', this.token);
			xhttp.send();
			this.spinner.visible = true;
		} catch(e) {
			alert(e);
		}
	}


	readNextPart() {
		try {
			this.isAuth();
			this.isOnLine();

			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = () => {
				if (xhttp.readyState === 4 && xhttp.status === 200) {
					let gotTasks = JSON.parse(xhttp.responseText);
					
					// this.tasks.splice(0, this.tasks.length);
					gotTasks.map((gotTask) => {
						this.tasks.push(gotTask);
					});
					this.spinner.visible = false;
					this.part += 1;
				};
				if (xhttp.readyState === 4 && xhttp.status !== 200) {
					this.spinner.visible = false;
					alert("Error. Status code: " + xhttp.status);
				}
			};
			xhttp.open("GET", "http://52.32.192.195:3003/taskList/" + this.part + "/" + this.size);
			xhttp.setRequestHeader('token', this.token);
			xhttp.send();
			this.spinner.visible = true;
		} catch(e) {
			alert(e);
		}
	}

	update(index) {
		try {
			this.isAuth();
			this.isOnLine();
		
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = () => {
				if (xhttp.readyState === 4 && xhttp.status === 200) {
					console.log('update in remoteDb: ' + xhttp.responseText);
					this.spinner.visible = false;
				}
				if (xhttp.readyState === 4 && xhttp.status !== 200) {
					this.spinner.visible = false;
					alert("Error. Status code: " + xhttp.status);
				}
			};
			xhttp.open("PUT", "http://52.32.192.195:3003/taskList/" + this.tasks[index].id);
			xhttp.setRequestHeader('Content-Type', 'application/json');
			xhttp.setRequestHeader('token', this.token);
			xhttp.send(JSON.stringify(this.tasks[index]));
			this.spinner.visible = true;
		} catch(e) {
			alert(e);
		}
	}

	delete(index) {
		try {
			this.isAuth();
			this.isOnLine();
		
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = () => {
				if (xhttp.readyState === 4 && xhttp.status === 200) {
					console.log('delete in remoteDb: ' + xhttp.responseText);
					this.spinner.visible = false;
				}
				if (xhttp.readyState === 4 && xhttp.status !== 200) {
					this.spinner.visible = false;
					alert("Error. Status code: " + xhttp.status);
				}
			};
			xhttp.open("DELETE", "http://52.32.192.195:3003/taskList/" + this.tasks[index].id);
			xhttp.setRequestHeader('token', this.token);
			xhttp.send();
			this.spinner.visible = true;
		} catch(e) {
			alert(e);
		}
	}
}
