import PouchDB from 'pouchdb';

export class LocalDb {
    constructor(tasks) {
        this.db = new PouchDB('taskList');
        this.tasks = tasks;
    }

    cleanDb() {
        this.db.destroy((err, response) => {
            if (err) {
                return console.log(err);
            } else {
                this.db =  new PouchDB('taskList');
                console.log('delete all in localDb: ' + JSON.stringify(response));// handle response
            }
        });

    }

    writeToLocalDB() {
        this.db.bulkDocs([
            {_id: 'doc1', title: 'Task1', description: 'it1 description description descr', photo: 'photo', barcode: 'barcode' },
            {_id: 'doc2', title: 'Task2', description: 'it1 descr', photo: 'photo', barcode: 'barcode' },
            {_id: 'doc3', title: 'Task3', description: 'it1 descr', photo: 'photo', barcode: 'barcode' }
        ], (err, response) => {
            if (err) { return console.log(err); }
        });
    }
    
    create(index) {
        this.db.bulkDocs([this.tasks[index]], (err, response) => {
            if (err) { return alert("update err: " + err); }
            console.log('update in localDb: ' + JSON.stringify(response));// handle response
            this.tasks[index]._id = response[0].id;
            this.tasks[index]._rev = response[0].rev;
        });
    }
    
    read() {
        this.db.allDocs({
            include_docs: true,
            attachments: true
        }, (err, response) => {
            if (err) { return console.log(err); }

            var gotTasks = response.rows;

            gotTasks.map((gotTask)=>{
                this.tasks.push(gotTask.doc);
            })
            console.log('read in localDb: ' + JSON.stringify(this.tasks));// handle response
        });    
    }

    update(index) {
        this.db.bulkDocs([this.tasks[index]], (err, response) => {
            if (err) { return alert("update err: " + err); }
            console.log('update in localDb: ' + JSON.stringify(response));// handle response
            this.tasks[index]._rev = response[0].rev;
        });
    }

    delete(index) {
        this.db.remove(this.tasks[index]._id, this.tasks[index]._rev, (err, response) => {
            if (err) { return console.log(err); }
            console.log('delete in localDb: ' + JSON.stringify(response));// handle response
        });
    }
}

