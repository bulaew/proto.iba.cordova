export function onDeviceReady() {
	window.onerror = function(errorMsg, url, lineNumber) {
	    window.fabric.Crashlytics.addLog("about to send a non fatal crash for testing! Error " + errorMsg + ". Url " + url + ". Linenumber " + lineNumber);
     	window.fabric.Crashlytics.sendNonFatalCrash("  Error " + errorMsg + ". Url " + url + ". Linenumber " + lineNumber);
		// window.fabric.Crashlytics.sendNonFatalCrash("Error " + errorMsg + ". Url " + url + ". Linenumber " + lineNumber);
		console.log('window.onerror ' + "  Error " + errorMsg + ". Url " + url + ". Linenumber " + lineNumber);
		return true;
	};
};
