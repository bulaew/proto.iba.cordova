export class Barcode {
    constructor(that) {
        this.that = that;
    }

    scanBarcode() {
        cordova.plugins.barcodeScanner.scan(
            (result) => this.scanBarcodeSuccess(result),
            (error) => {
                alert("Scanning failed: " + error);
            },
            {
                preferFrontCamera : false, // iOS and Android
                showFlipCameraButton : true, // iOS and Android
                prompt : "Place a barcode inside the scan area", // supported on Android only
                formats : "QR_CODE,DATA_MATRIX,UPC_E,UPC_A,EAN_8,EAN_13,CODE_128,CODE_39,CODE_93,CODABAR,ITF,RSS14,RSS_EXPANDED,PDF417,AZTEC", // default: all but PDF_417 and RSS_EXPANDED
                orientation : "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
            }
        );
    }

    scanBarcodeSuccess(result) {
        if(!result.cancelled) {
            this.that.barcode = result.format + ': ' + result.text;
        }
    }
}

