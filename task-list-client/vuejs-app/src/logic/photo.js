export class Photo {
    constructor(that) {
        this.that = that;
    }

    getPicture() {
        navigator.camera.getPicture((pathToFile) => this.onSuccess(pathToFile), this.onFail);
    }

    onSuccess(pathToFile) {
        this.getFile(pathToFile);
    }

    onFail(message) {
        alert('Failed because: ' + message);
    }

    getFile(pathToFile) {
        window.resolveLocalFileSystemURL(pathToFile, (fileEntry) => this.gotFile(fileEntry), this.fail);
    }

    gotFile(fileEntry) {
        fileEntry.file((file) => {
            let reader = new FileReader();

            reader.onloadend = (e) => {
                this.loadEndPhoto(reader.result);
            }
            reader.readAsBinaryString(file);
        });
    }

    fail(e) {
        console.log("FileSystem Error");
        console.dir(e);
    }
    
    loadEndPhoto(data){
        this.that.photo = 'data:image/jpeg;base64,' + btoa(data);
    }
    
}
