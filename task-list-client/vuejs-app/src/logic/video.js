export class Video {
    constructor(that) {
        this.that = that;
    }

    captureVideo() {
        navigator.device.capture.captureVideo((mediaFiles) => this.captureVideoSuccess(mediaFiles), this.captureError, {limit:1});
    }

    captureVideoSuccess(mediaFiles) {
        let pathToFile;

        pathToFile = mediaFiles[0].fullPath;
        this.getFile(pathToFile);
    }

    captureError(error) {
        console.log('Error code: ' + error.code, null, 'Capture Error');
    }
            
    getFile(pathToFile) {
        window.resolveLocalFileSystemURL(pathToFile, (fileEntry) => this.gotFile(fileEntry), this.fail);
    }

    gotFile(fileEntry) {
        fileEntry.file((file) => {
            let reader = new FileReader();

            reader.onloadend = (e) => {
                this.loadEndVideo(reader.result);
            }
            reader.readAsBinaryString(file);
        });
    }

    fail(e) {
        console.log("FileSystem Error");
        console.dir(e);
    }

    loadEndVideo(data) {
        this.that.video = 'data:video/mp4;base64,' + btoa(data);
    }
    
}
