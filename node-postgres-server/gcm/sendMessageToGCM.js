var gcm = require('node-gcm');

// Set up the sender with your GCM/FCM API key (declare this once for multiple messages)
var sender = new gcm.Sender('AIzaSyDpKg74WQr8y-wi8AbsxqKjAWk7Yt8ZrjM');

// Prepare a message to be sent
var message = new gcm.Message({
    collapseKey: 'demo',
    priority: 'high',
    contentAvailable: true,
    delayWhileIdle: true,
    timeToLive: 3,
    restrictedPackageName: "iba.cz.by",
    dryRun: false,
    data: {
        key1: 'message1',
        key2: 'message2'
    },
    notification: {
        title: "Hello, World",
        icon: "ic_launcher",
        body: "This is a notification that will be displayed ASAP."
    }
});
 
// Specify which registration IDs to deliver the message to
var registrationTokens = ['ctN4yqPvFGA:APA91bH7N9p6B2_AN8ZSDTEpHVYHLrt1YRPQwZAqknOHNrRPPD4NE73NM_qV5nfIglOtWLHQZkKmNa_1hW-Ih-wTwv3z3e7s2pNkQQGP-LsFgt9AWXE22MnbjvtVDhJUomsBr9UOKSSd'];

// Actually send the message
sender.send(message, { registrationTokens: registrationTokens }, 10, function (err, response) {
  if(err) console.error(err);
  else    console.log(response);
});