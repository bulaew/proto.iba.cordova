const express = require('express');
const router = express.Router();
const pg = require('pg');
const path = require('path');
const connectionString = process.env.DATABASE_URL || 'postgres://test1:test1@127.0.0.1:5432/postgres';
const jwt = require('jsonwebtoken');

router.use(function(req, res, next) {

  let token = req.headers.token;

  if (token) {

    jwt.verify(token, 'secret', function(err, decoded) {      
      if (err) {
        return res.status(401).json({ success: false, message: 'Unauthorized.' });    
      } else {
        req.decodedToken = decoded;    
        next();
      }
    });

  } else {

    return res.status(401).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});


router.post('/', (req, res, next) => {
  if(!req.decodedToken.admin) {
      return res.status(403).json({success: false, message: 'Forbidden.'});
  }

  const data = {
    title: req.body.title,
    description: req.body.description,
    photo: req.body.photo,
    video: req.body.video,
    barcode: req.body.barcode,
  };

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      return res.status(500).json(err);
    }

    client.query('INSERT INTO taskList(title, description, photo, video, barcode) values($1, $2, $3, $4, $5)',
    [data.title, data.description, data.photo, data.video, data.barcode]);
    client.query('SELECT * FROM taskList ORDER BY id ASC;',    
    (err, result) => {
      done();
      if(err) {
        res.status(500).json(err);
      } else {
        res.json(result.rows);
      }
    });
  });
});

router.post('array/', (req, res, next) => {
  if(!req.decodedToken.admin) {
      return res.status(403).json({success: false, message: 'Forbidden.'});
  }

  const dataArray = req.body;

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      return res.status(500).json(err);
    }

    dataArray.forEach((data)=> {
      client.query('INSERT INTO taskList(title, description, photo, video, barcode) values($1, $2, $3, $4, $5)',
      [data.title, data.description, data.photo, data.video, data.barcode]);
    })
  });
});

router.get('/', (req, res, next) => {

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      return res.status(500).json(err);
    }

    client.query('SELECT * FROM taskList ORDER BY id ASC;',
    (err, result) => {
      done();
      if(err) {
        res.status(500).json(err);
      } else {
        res.json(result.rows);
      }
    });
  });
});

router.put('/:task_id', (req, res, next) => {
  const id = req.params.task_id;
  const data = {
    title: req.body.title,
    description: req.body.description,
    photo: req.body.photo,
    video: req.body.video,
    barcode: req.body.barcode,
  };

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      return res.status(500).json(err);
    }

    client.query('UPDATE taskList SET title=($1), description=($2), photo=($3), video=($4), barcode=($5) WHERE id=($6)',
    [data.title, data.description, data.photo, data.video, data.barcode, id],
    (err, result) => {
      done();
      if(err) {
        res.status(500).json(err);
      } else {
        res.json(result);
      }
    });
  });
});

router.delete('/:task_id', (req, res, next) => {
  if(!req.decodedToken.admin) {
      return res.status(403).json({success: false, message: 'Forbidden.'});
  }

  const id = req.params.task_id;

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      return res.status(500).json(err);
    }

    client.query('DELETE FROM taskList WHERE id=($1)', 
    [id],
    (err, result) => {
      done();
      if(err) {
        res.status(500).json(err);
      } else {
        res.json(result);
      }
    });
  });
});

module.exports = router;
