const express = require('express');
const router = express.Router();
const pg = require('pg');
const path = require('path');
const connectionString = process.env.DATABASE_URL || 'postgres://test1:test1@127.0.0.1:5432/postgres';
var jwt = require('jsonwebtoken');

router.post('/login', (req, res, next) => {
  const user = {
    name: req.body.name,
    password: req.body.password
  };

  pg.connect(connectionString, (err, client, done) => {

    if(err) {
      done();
      console.log('err:' + err);
      return res.status(500).json({success: false, message: err});
    }

    client.query('SELECT * FROM userList WHERE name=$1',
    [user.name], 
    (err, result) => {
      
      if(err) {
        done();
        console.log('err:' + err);
        return res.status(500).json({success: false, message: err});
      }
      
      if (!result.rows.length){
        res.status(401).json({
          success: false,
          message: 'Wrong username!',
        });  

      } else {
        let row = result.rows[0];
        
        if (row.password !== user.password) {

          res.status(401).json({
            success: false,
            message: 'Wrong password!',
          });

        } else {
          
          jwt.sign(row, 'secret', { algorithm: 'HS256' }, (err, token) => {
       		console.log("send resp");     

	    res.json({
              success: true,
              message: 'Correct token!',
              name: row.name,
              admin: row.admin,
              token: token
            });
          });

        }
      }

    });

  });
});

module.exports = router;
