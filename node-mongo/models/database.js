var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var fs = require('fs');
  
// Connection URL 
var url = 'mongodb://localhost:27017/myproject';

var mockData = {}; 
loadMockData();

var usersData = {};
// loadUsersData();


// Use connect method to connect to the Server 
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
  
  insertCounters(db);
  // getNextSequence(db, "docId",function(err, doc){
  //     console.dir(doc);
  //     findData(db, "counters", {_id:"docId"});
  //     db.close();
  // });


  // deleteData(db, "documents", {});
  insertDocuments(db,mockData);
  insertUsers(db,usersData);
  findData(db, "documents",{});
  db.close();
  
  // findUser(db, {name: 'admin',
  //   password: '8asc6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
  //   admin: true });
 
});


function insertCounters(db) {
  // Get the documents collection 
  var collection = db.collection('counters');
  // Insert some documents 
  collection.insertMany([{
      _id: "docId",
      seq: 52
    }],
    function(err, result) {
      assert.equal(err, null);
      console.log("Inserted counters into the document collection");
    });
}

function getNextSequence(db, name, callback) {
  var collection = db.collection('counters');

  collection.findAndModify(
    { "_id": name },
    [ ["_id", 1] ], 
    { "$inc": { "seq": 1 } },
    { "new": true },
    callback);
}


function findData(db, collectionName, object) {
  // Get the documents collection 
  var collection = db.collection(collectionName);
  // Find some documents 
  collection.find(object).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.dir(docs,{depth:6});
    
  });
}

function deleteData(db, collectionName, object) {
  // Get the documents collection 
  var collection = db.collection(collectionName);
  // Insert some documents 
  collection.deleteMany(object, function(err, result) {
    assert.equal(err, null);
    console.log("Removed the document with the field a equal to 3");
    
  });
}
 
function insertDocuments(db, data) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Insert some documents 
  collection.insertMany(data,
    function(err, result) {
      assert.equal(err, null);
      console.log("Inserted "+result.length+" documents into the document collection");
    });
}

function insertDocument(db, data) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Insert some documents 
  collection.insertMany([data],
    function(err, result) {
      assert.equal(err, null);
      console.log("Inserted 1 documents into the document collection");
    });
}


function updateDocument(db, docA, docB, callback) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Update document where a is 2, set b equal to 1 
  collection.updateOne( 
    docA, 
    { $set: docB }, function(err, result) {
      assert.equal(err, null);
      console.log("Updated the document with the field ",docA, docB);
      callback(result);
  });  
}
function deleteDocument(db, id) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Insert some documents 
  collection.deleteOne({id: id}, function(err, result) {
    assert.equal(err, null);
    console.log("Removed the document with the field a equal to 3");
    
  });
}




function insertUsers(db, data) {
  // Get the documents collection 
  var collection = db.collection('users');
  // Insert some documents 
  collection.insertMany(data,
    function(err, result) {
      assert.equal(err, null);
      console.log("Inserted 3 documents into the document collection");
      
    });
}

function findUser(db, user) {
  // Get the documents collection 
  var collection = db.collection('users');
  // Find some documents 
  collection.find(user).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.dir(docs,{depth:6});
    
  });
}


// Sync:

function loadMockData() {
  mockData = JSON.parse(fs.readFileSync('./MOCK_DATA.json', 'utf8'));
  console.log("mockData ");
  console.dir(mockData, {depth:6});
}

function loadUsersData() {
  usersData = JSON.parse(fs.readFileSync('./USERS_DATA.json', 'utf8'));
  console.log("users data ");
  console.dir(usersData, {depth:6});
}

// Async:

function loadMockDataAsync() {
  var obj;
  fs.readFile('MOCK_DATA', 'utf8', function (err, data) {
    if (err) throw err;
    mockData = JSON.parse(data);
  });
}

