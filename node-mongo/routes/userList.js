const express = require('express');
const router = express.Router();
const path = require('path');
const jwt = require('jsonwebtoken');

var assert = require('assert');
var MongoClient = require('mongodb').MongoClient
var url = 'mongodb://localhost:27017/myproject';

router.post('/login', (req, res, next) => {
  const user = {
    name: req.body.name,
    password: req.body.password
  };

  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    getToken(db, user, res);

    db.close();
  });

});

module.exports = router;


function getToken(db, user, res) {
  // Get the documents collection 
  var collection = db.collection('users');
  // Find some documents 
  collection.find(user).toArray(function(err, docs) {
    if(err) {
      console.log('err:' + err);
      return res.status(500).json({success: false, message: err});
    }
    
    if (!docs.length){
      res.status(401).json({
        success: false,
        message: 'Wrong username!',
      });  
    } else {
      var doc = docs[0];

      jwt.sign(doc, 'secret', { algorithm: 'HS256' }, (err, token) => {
        res.json({
          success: true,
          message: 'Correct token!',
          name: doc.name,
          admin: doc.admin,
          token: token
        });
      });
    }
    console.log("Found the following records");
    console.dir(docs,{depth:6});
  });
}
