const express = require('express');
const router = express.Router();
const path = require('path');
const jwt = require('jsonwebtoken');

var assert = require('assert');
var MongoClient = require('mongodb').MongoClient
var url = 'mongodb://localhost:27017/myproject';

router.use(function(req, res, next) {

  let token = req.headers.token;

  if (token) {

    jwt.verify(token, 'secret', function(err, decoded) {      
      if (err) {
        return res.status(401).json({ success: false, message: 'Unauthorized.' });    
      } else {
        req.decodedToken = decoded;    
        next();
      }
    });

  } else {

    return res.status(401).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});


router.post('/', (req, res, next) => {
  if(!req.decodedToken.admin) {
      return res.status(403).json({success: false, message: 'Forbidden.'});
  }


  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    getNextSequence(db, "docId",function(err, doc){

      const data = {
        id: doc.value.seq,
        title: req.body.title,
        description: req.body.description,
        photo: req.body.photo,
        video: req.body.video,
        barcode: req.body.barcode,
      };
      
      console.dir(data);

      insertDocument(db, data, function(err, result) {
        assert.equal(err, null);
        console.log("Inserted 1 documents into the document collection"+JSON.stringify(data));
        res.json(data);
        db.close();
      });
    });



  });

});

router.get('/', (req, res, next) => {

  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    findDocuments(db, (result) => {
      result.sort(function(a,b){
        return b.id - a.id;
      });
      res.json(result);
      db.close();
    })

  });
});


router.get('/:part/:size', (req, res, next) => {
  const part = parseInt(req.params.part);
  const size = parseInt(req.params.size);

  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    findDocuments(db, (result) => {
      result.sort(function(a,b){
        return b.id - a.id;
      });
      result = result.slice(part*size, part*size + size);
      res.json(result);
      db.close();
    })

  });
});

router.put('/:task_id', (req, res, next) => {
  const id = parseInt(req.params.task_id);
  const data = {
    title: req.body.title,
    description: req.body.description,
    photo: req.body.photo,
    video: req.body.video,
    barcode: req.body.barcode,
  };

  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server",id,data);

    updateDocument(db, {id: id}, data, (result) => {
      res.json(data);
      db.close();
    })

  });

});

router.delete('/:task_id', (req, res, next) => {
  if(!req.decodedToken.admin) {
      return res.status(403).json({success: false, message: 'Forbidden.'});
  }

  const id = parseInt(req.params.task_id);

 
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    deleteDocument(db, {id: id}, (result) => {
      res.json(id);
      db.close();
    })

  });
});

module.exports = router;

function getNextSequence(db, name, callback) {
  var collection = db.collection('counters');

  collection.findAndModify(
    { "_id": name },
    [ ["_id", 1] ], 
    { "$inc": { "seq": 1 } },
    { "new": true },
    callback);
}

function insertDocument(db, data, callback) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Insert some documents 
  collection.insertMany([data], callback);
}

function findDocuments(db, callback) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Find some documents 
  collection.find({}).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    callback(docs);
  });
}

function updateDocument(db, docA, docB, callback) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Update document where a is 2, set b equal to 1 
  collection.updateOne( 
    docA, 
    { $set: docB }, function(err, result) {
    assert.equal(err, null);
    console.log("Updated the document with the field ",docA, docB);
    callback(result);
  });  
}

function deleteDocument(db, obj, callback) {
  // Get the documents collection 
  var collection = db.collection('documents');
  // Insert some documents 
  collection.deleteOne(obj, function(err, result) {
    assert.equal(err, null);
    console.log("Removed the document with the field a equal ");
    callback(result);
  });
}
